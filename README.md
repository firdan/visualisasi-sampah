# Visualisasi Sampah


## Description
Small app for showing garbage visualization for administrative region level 4 ("kecamatan").

## Installation
required app:

* bower
* compass

installation:

1. go to root folder
2. run `bower install`
3. run `compass compile`
4. done!

## Credits
* Firdan Machda
* Tri
* Habibie
* Teguh