const CSS = [
    'bootstrap/dist/css/bootstrap.min.css',
    'colorbrewer/colorbrewer.css',
    'ion-rangeslider/css/ion.rangeSlider.min.css'
];

// const JS = [
//     'jquery-ui-dist/jquery-ui.min.js',
//     'jquery-datetimepicker/jquery.datetimepicker.min.js',
//     'jquery/dist/jquery.min.js',
//     'lodash/lodash.min.js'
// ];

module.exports = [...CSS];
