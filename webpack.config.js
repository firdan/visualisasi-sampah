const path = require('path');
const webpack = require('webpack');
const CopyPlugin = require('copy-webpack-plugin');

// assets.js
const Assets = require('./assets');


module.exports = {
    mode:'development',
    entry: {
        app: "./bandung_vis.js",
    },
    output: {
        path: path.resolve(__dirname , "public"),
        filename: "[name].bundle.js"
    },
    plugins: [
      new webpack.ProvidePlugin({
        $: 'jquery',
        jQuery: 'jquery',
      }),
      new CopyPlugin({
        patterns: [...Assets.map(asset => {
            return {
              from: path.resolve(__dirname, `node_modules/${asset}`),
              to: path.resolve(__dirname, 'public/node_modules')
            };
          }),
          {from:path.resolve(__dirname,'src'), to:path.resolve(__dirname,'public')},
          {from:path.resolve(__dirname,'node_modules/bootstrap/fonts'), to:path.resolve(__dirname,'public/fonts')}
        ],
        options: {
          concurrency:100,
        }
      })
    ]
};
